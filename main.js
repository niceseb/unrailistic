const DarkSkyNET = require('darksky-net');
const forecast = new DarkSkyNET('265c33176e950c4b87a0771a33d2acc8');
const mysql = require('mysql');

function weatherGet(lat, long, time) {
    return forecast
            .latitude(lat)                              // required: latitude, string. i.e. '37.8267'
            .longitude(long)                            // required: longitude, string. i.e. '-122.423'
            .time(time)                                 // optional: date, string 'YYYY-MM-DD'.
            .units('ca')                                // optional: units, string, refer to API documentation.
            .language('en')                             // optional: language, string, refer to API documentation.
            .exclude('hourly,daily,alerts,flags')       // optional: exclude, string, refer to API documentation.
            .get()                                      // execute your get request.
            .catch(err => {                             //handle your error response.
            console.error(err);
    throw err;
})
}

var express = require('express');
var app = express();

var connection = mysql.createConnection({
    host: "192.168.1.25",
    user: "root",
    password: "bash",
    port: "3306",
    database: "hacktrain"
});

connection.connect(function (err) {
    if (!err)
        console.log('Database is connected .... nn');
    else
        console.log('Error connecting database ... nn');
});

app.get("/", function (req, res) {
    const selectQuery = 'SELECT * FROM hacktrain.travel_data LIMIT 0, 1000';
    connection.query(selectQuery, function (err, rows, fields) {
        connection.end();
        if (!err)
            console.log('The solution is: ', rows);
        else
            console.log('Error while performing Query.');
    });
});


app.get('/:lat/:long/:time', function (req, res) {
    weatherGet(req.params.lat, req.params.long, req.params.time).then(response => res.send(response));
});

app.listen(8090);